# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
# Guðmundur Erlingsson <gudmundure@gmail.com>, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-09 01:39+0000\n"
"PO-Revision-Date: 2023-03-16 20:42+0000\n"
"Last-Translator: gummi <gudmundure@gmail.com>\n"
"Language-Team: Icelandic <kde-i18n-doc@kde.org>\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ../sddm-theme/KeyboardButton.qml:19
#, kde-format
msgid "Keyboard Layout: %1"
msgstr "Lyklaborðsuppsetning: %1"

#: ../sddm-theme/Login.qml:85
#, kde-format
msgid "Username"
msgstr "Notandanafn"

#: ../sddm-theme/Login.qml:102 contents/lockscreen/MainBlock.qml:64
#, kde-format
msgid "Password"
msgstr "Lykilorð"

#: ../sddm-theme/Login.qml:144 ../sddm-theme/Login.qml:150
#, kde-format
msgid "Log In"
msgstr "Skrá inn"

#: ../sddm-theme/Main.qml:201 contents/lockscreen/LockScreenUi.qml:316
#, kde-format
msgid "Caps Lock is on"
msgstr "Kveikt er á hástafalás (CapsLock)"

#: ../sddm-theme/Main.qml:213 ../sddm-theme/Main.qml:470
#: contents/logout/Logout.qml:165
#, kde-format
msgctxt "Suspend to RAM"
msgid "Sleep"
msgstr "Svæfa"

#: ../sddm-theme/Main.qml:220 ../sddm-theme/Main.qml:477
#: contents/logout/Logout.qml:185
#, kde-format
msgid "Restart"
msgstr "Endurræsa"

#: ../sddm-theme/Main.qml:227 ../sddm-theme/Main.qml:484
#: contents/logout/Logout.qml:196
#, kde-format
msgid "Shut Down"
msgstr "Slökkva á tölvunni"

#: ../sddm-theme/Main.qml:234
#, kde-format
msgctxt "For switching to a username and password prompt"
msgid "Other…"
msgstr "Annað…"

#: ../sddm-theme/Main.qml:456
#, kde-format
msgid "Type in Username and Password"
msgstr "Sláðu inn notandanafn og lykilorð"

#: ../sddm-theme/Main.qml:491
#, kde-format
msgid "List Users"
msgstr "Telja upp notendur"

#: ../sddm-theme/Main.qml:566 contents/lockscreen/LockScreenUi.qml:591
#, kde-format
msgctxt "Button to show/hide virtual keyboard"
msgid "Virtual Keyboard"
msgstr "Sýndarlyklaborð"

#: ../sddm-theme/Main.qml:640
#, kde-format
msgid "Login Failed"
msgstr "Innskráning mistókst"

#: ../sddm-theme/SessionButton.qml:18
#, kde-format
msgid "Desktop Session: %1"
msgstr "Skjáborðsseta: %1"

#: contents/lockscreen/config.qml:19
#, kde-format
msgctxt "@title: group"
msgid "Clock:"
msgstr "Klukka:"

#: contents/lockscreen/config.qml:22
#, kde-format
msgctxt "@option:check"
msgid "Keep visible when unlocking prompt disappears"
msgstr "Halda sýnilegu þegar aflæsingarkvaðning hverfur"

#: contents/lockscreen/config.qml:33
#, kde-format
msgctxt "@title: group"
msgid "Media controls:"
msgstr "Margmiðlunarstýringar:"

#: contents/lockscreen/config.qml:36
#, kde-format
msgctxt "@option:check"
msgid "Show under unlocking prompt"
msgstr "Sýna undir aflæsingarkvaðningu"

#: contents/lockscreen/LockScreenUi.qml:67
#, kde-format
msgid "Unlocking failed"
msgstr "Aflæsing tókst ekki"

#: contents/lockscreen/LockScreenUi.qml:330
#, kde-format
msgid "Sleep"
msgstr "Svæfa"

#: contents/lockscreen/LockScreenUi.qml:336 contents/logout/Logout.qml:175
#, kde-format
msgid "Hibernate"
msgstr "Leggja í dvala"

#: contents/lockscreen/LockScreenUi.qml:342
#, kde-format
msgid "Switch User"
msgstr "Skipta um notanda"

#: contents/lockscreen/LockScreenUi.qml:529
#, kde-format
msgid "Switch to This Session"
msgstr "Skipta yfir í þessa setu"

#: contents/lockscreen/LockScreenUi.qml:537
#, kde-format
msgid "Start New Session"
msgstr "Hefja nýja setu"

#: contents/lockscreen/LockScreenUi.qml:550
#, kde-format
msgid "Back"
msgstr "Til baka"

#: contents/lockscreen/LockScreenUi.qml:615
#, kde-format
msgctxt "Button to change keyboard layout"
msgid "Switch layout"
msgstr "Skipta um framsetningu"

#: contents/lockscreen/MainBlock.qml:107
#: contents/lockscreen/NoPasswordUnlock.qml:17
#, kde-format
msgid "Unlock"
msgstr "Aflæsa"

#: contents/lockscreen/MainBlock.qml:155
#, kde-format
msgid "(or scan your fingerprint on the reader)"
msgstr ""

#: contents/lockscreen/MainBlock.qml:159
#, kde-format
msgid "(or scan your smartcard)"
msgstr ""

#: contents/lockscreen/MediaControls.qml:60
#, kde-format
msgid "No title"
msgstr "Enginn titill"

#: contents/lockscreen/MediaControls.qml:61
#, kde-format
msgid "No media playing"
msgstr "Ekkert efni í spilun"

#: contents/lockscreen/MediaControls.qml:89
#, kde-format
msgid "Previous track"
msgstr "Fyrra lag"

#: contents/lockscreen/MediaControls.qml:101
#, kde-format
msgid "Play or Pause media"
msgstr "Spila eða setja á pásu"

#: contents/lockscreen/MediaControls.qml:111
#, kde-format
msgid "Next track"
msgstr "Næsta lag"

#: contents/logout/Logout.qml:140
#, kde-format
msgid ""
"One other user is currently logged in. If the computer is shut down or "
"restarted, that user may lose work."
msgid_plural ""
"%1 other users are currently logged in. If the computer is shut down or "
"restarted, those users may lose work."
msgstr[0] ""
"Einn annar notandi er ennþá skráður inn. Ef slökkt er á tölvunni eða hún "
"endurræst gæti viðkomandi tapað gögnum."
msgstr[1] ""
"%1 aðrir notendur eru ennþá skráðir inn. Ef slökkt er á tölvunni eða hún "
"endurræst gætu viðkomandi tapað gögnum."

#: contents/logout/Logout.qml:154
#, kde-format
msgid "When restarted, the computer will enter the firmware setup screen."
msgstr ""
"Við endurræsingu mun tölvan fara á skjáinn fyrir uppsetningu fastbúnaðar "
"(firmware)."

#: contents/logout/Logout.qml:207
#, kde-format
msgid "Log Out"
msgstr "Skrá út"

#: contents/logout/Logout.qml:231
#, kde-format
msgid "Restarting in 1 second"
msgid_plural "Restarting in %1 seconds"
msgstr[0] "Endurræsi eftir 1 sekúndu"
msgstr[1] "Endurræsi eftir %1 sekúndur"

#: contents/logout/Logout.qml:233
#, kde-format
msgid "Shutting down in 1 second"
msgid_plural "Shutting down in %1 seconds"
msgstr[0] "Slekk eftir 1 sekúndu"
msgstr[1] "Slekk eftir %1 sekúndur"

#: contents/logout/Logout.qml:235
#, kde-format
msgid "Logging out in 1 second"
msgid_plural "Logging out in %1 seconds"
msgstr[0] "Útskráning eftir 1 sekúndu"
msgstr[1] "Útskráning eftir %1 sekúndur"

#: contents/logout/Logout.qml:247
#, kde-format
msgid "OK"
msgstr "Í lagi"

#: contents/logout/Logout.qml:259
#, kde-format
msgid "Cancel"
msgstr "Hætta við"

#: contents/osd/OsdItem.qml:32
#, kde-format
msgctxt "Percentage value"
msgid "%1%"
msgstr "%1%"

#: contents/splash/Splash.qml:79
#, kde-format
msgctxt ""
"This is the first text the user sees while starting in the splash screen, "
"should be translated as something short, is a form that can be seen on a "
"product. Plasma is the project name so shouldn't be translated."
msgid "Plasma made by KDE"
msgstr "Plasma gert af KDE"

#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery at %1%"
#~ msgstr "Rafhlaða í %1%"

#~ msgctxt "Nobody logged in on that session"
#~ msgid "Unused"
#~ msgstr "Ónotað"

#~ msgctxt "User logged in on console number"
#~ msgid "TTY %1"
#~ msgstr "TTY %1"

#~ msgctxt "User logged in on console (X display number)"
#~ msgid "on TTY %1 (Display %2)"
#~ msgstr "á TTY %1 (skjár %2)"

#~ msgctxt "Username (location)"
#~ msgid "%1 (%2)"
#~ msgstr "%1 (%2)"

#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "Configure"
#~ msgstr "Stilla"

#~ msgid "Configure KRunner Behavior"
#~ msgstr "Stilla hegðun KRunner"

#~ msgid "Configure KRunner…"
#~ msgstr "Stilla KRunner…"

#~ msgctxt "Textfield placeholder text, query specific KRunner"
#~ msgid "Search '%1'…"
#~ msgstr "Leita að '%1'…"

#~ msgctxt "Textfield placeholder text"
#~ msgid "Search…"
#~ msgstr "Leita…"

#~ msgid "Show Usage Help"
#~ msgstr "Birta hjálp varðandi notkun"

#~ msgid "Pin"
#~ msgstr "Festa"

#~ msgid "Pin Search"
#~ msgstr "Festa leit"

#~ msgid "Keep Open"
#~ msgstr "Halda opnu"

#~ msgid "Recent Queries"
#~ msgstr "Nýlegar fyrirspurnir"

#~ msgid "Remove"
#~ msgstr "Fjarlægja"

#~ msgid "in category recent queries"
#~ msgstr "í flokknum nýlegar fyrirspurnir"
