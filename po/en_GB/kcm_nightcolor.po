# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Steve Allewell <steve.allewell@gmail.com>, 2017, 2019, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-14 01:39+0000\n"
"PO-Revision-Date: 2022-09-10 11:05+0100\n"
"Last-Translator: Steve Allewell <steve.allewell@gmail.com>\n"
"Language-Team: British English <kde-l10n-en_gb@kde.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.08.0\n"

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Tap to choose your location on the map."

#: ui/LocationsFixedView.qml:40
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Click to choose your location on the map."

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "Zoom in"

#: ui/LocationsFixedView.qml:222
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:235
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Latitude:"

#: ui/LocationsFixedView.qml:261
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Longitude:"

#: ui/main.qml:102
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "The blue light filter makes the colours on the screen warmer."

#: ui/main.qml:113
#, kde-format
msgid "Switching times:"
msgstr "Switching times:"

#: ui/main.qml:115
#, kde-format
msgid "Always off"
msgstr "Always off"

#: ui/main.qml:116
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Sunset and sunrise at current location"

#: ui/main.qml:117
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Sunset and sunrise at manual location"

#: ui/main.qml:118
#, kde-format
msgid "Custom times"
msgstr "Custom times"

#: ui/main.qml:119
#, fuzzy, kde-format
#| msgid "Always on night color"
msgid "Always on night light"
msgstr "Always on night colour"

#: ui/main.qml:171
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."

#: ui/main.qml:190
#, fuzzy, kde-format
#| msgid "Day color temperature:"
msgid "Day light temperature:"
msgstr "Day colour temperature:"

#: ui/main.qml:233 ui/main.qml:292
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1K"

#: ui/main.qml:237 ui/main.qml:296
#, fuzzy, kde-format
#| msgctxt "No blue light filter activated"
#| msgid "Cool (no filter)"
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Cool (no filter)"

#: ui/main.qml:243 ui/main.qml:302
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Warm"

#: ui/main.qml:249
#, fuzzy, kde-format
#| msgid "Night color temperature:"
msgid "Night light temperature:"
msgstr "Night colour temperature:"

#: ui/main.qml:313
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Latitude: %1°   Longitude: %2°"

#: ui/main.qml:323
#, fuzzy, kde-format
#| msgid "Begin night color at:"
msgid "Begin night light at:"
msgstr "Begin night colour at:"

#: ui/main.qml:336 ui/main.qml:359
#, kde-format
msgid "Input format: HH:MM"
msgstr "Input format: HH:MM"

#: ui/main.qml:346
#, fuzzy, kde-format
#| msgid "Begin day color at:"
msgid "Begin day light at:"
msgstr "Begin day colour at:"

#: ui/main.qml:368
#, kde-format
msgid "Transition duration:"
msgstr "Transition duration:"

#: ui/main.qml:377
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minute"
msgstr[1] "%1 minutes"

#: ui/main.qml:390
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Input minutes - min. 1, max. 600"

#: ui/main.qml:408
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Error: Transition time overlaps."

#: ui/main.qml:430
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Locating…"

#: ui/TimingsView.qml:32
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Colour temperature begins changing to night time at %1 and is fully changed "
"by %2"

#: ui/TimingsView.qml:39
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Colour temperature begins changing to day time at %1 and is fully changed by "
"%2"

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Cool"

#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "This is what day colour temperature will look like when active."

#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "This is what night colour temperature will look like when active."

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Activate Night Colour"

#~ msgid "Turn on at:"
#~ msgstr "Turn on at:"

#~ msgid "Turn off at:"
#~ msgstr "Turn off at:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "Night Colour begins changing back at %1 and ends at %2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Error: Morning is before evening."

#~ msgid "Detect Location"
#~ msgstr "Detect Location"

#~ msgid ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."
#~ msgstr ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."

#~ msgid "Night Color begins at %1"
#~ msgstr "Night Colour begins at %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "Colour fully changed at %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "Normal colouration restored by %1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Steve Allewell"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "steve.allewell@gmail.com"

#~ msgid "Night Color"
#~ msgstr "Night Colour"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr " K"

#~ msgid "Operation mode:"
#~ msgstr "Operation mode:"

#~ msgid "Automatic"
#~ msgstr "Automatic"

#~ msgid "Times"
#~ msgstr "Times"

#~ msgid "Constant"
#~ msgstr "Constant"

#~ msgid "Sunrise begins:"
#~ msgstr "Sunrise begins:"

#~ msgid "(Input format: HH:MM)"
#~ msgstr "(Input format: HH:MM)"

#~ msgid "Sunset begins:"
#~ msgstr "Sunset begins:"

#~ msgid "...and ends:"
#~ msgstr "...and ends:"

#~ msgid "Manual"
#~ msgstr "Manual"

#~ msgid "(HH:MM)"
#~ msgstr "(HH:MM)"
